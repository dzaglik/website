import { defineCollection, z } from 'astro:content';

const mainPagesCollection = defineCollection({
    type: 'data',
    schema: ({ image }) => z.object({
        title: z.string(),
        subtitle: z.string(),
        description: z.string(),
        blocks: z.array(z.object({
            title: z.string(),
            description: z.string(),
            link: z.string(),
            img: z.object({
                file: image(),
                alt: z.string(),
            }),
        })),
    }),
});

const foodPagesCollection = defineCollection({
    type: 'data',
    schema: ({ image }) => z.object({
        title: z.string(),
        content: z.string(),
        images: z.array(z.object({
            file: image(),
            alt: z.string(),
        })),
    }),
});

const medsPagesCollection = defineCollection({
    type: 'data',
    schema: z.object({
        title: z.string(),
        description: z.string(),
        medicines: z.object({
            pharmacy_products: z.array(z.string()),
            veterinary_products: z.array(z.string()),
        }),
    }),
});

export const collections = {
    'food': foodPagesCollection,
    'meds': medsPagesCollection,
    'main': mainPagesCollection,
};
