export declare interface MainCardData {
    title: string;
    description: string;
    imgSrc: ImageMetadata;
    link?: string;
}
//google tag trick
declare global {
    interface Window {
        dataLayer?: any;
        DonorBox: {
            widgetLinkClassName: string;
        };
    }
  }
