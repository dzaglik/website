# Development

## Setup
You'll need to have the following installed on your machine:
- [Node.js](https://nodejs.org/en/)
- [pnpm](https://pnpm.io/)

### Pull the repository
```bash
git clone https://gitlab.com/petshelter/website.git
```

### Install dependencies
```bash
pnpm install
```

### Start the development server
```bash
pnpm dev
```

### Open the browser
Navigate to [http://localhost:4321](http://localhost:4321) to see the website.

