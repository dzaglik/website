import sitemap from "@astrojs/sitemap";
import tailwind from "@astrojs/tailwind";
import vue from "@astrojs/vue";
import compress from "astro-compress";
import robotsTxt from "astro-robots-txt";
import { defineConfig } from 'astro/config';

const siteURL = process.env.SITE_URL || 'http://localhost:4321';

// https://astro.build/config
export default defineConfig({
  site: siteURL,
  integrations: [tailwind(), vue(), compress({
    HTML: false
  }), sitemap({
    i18n: {
      defaultLocale: 'ru',
      locales: {
        ru: 'ru-RU',
        en: 'en-US'
      }
    }
  }), robotsTxt()]
});